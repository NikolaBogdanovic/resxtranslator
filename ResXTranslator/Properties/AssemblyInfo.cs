﻿using System;
using System.Reflection;
using System.Resources;
using System.Runtime.InteropServices;

[assembly: CLSCompliant(true)]
[assembly: ComVisible(false)]
[assembly: Guid("1eb42955-ce18-484e-b1c1-40c9f0f81f06")]
[assembly: AssemblyVersion("1.0.0.0")]
[assembly: AssemblyFileVersion("1.0.0.0")]
[assembly: AssemblyDefaultAlias("ResX Translator")]
[assembly: AssemblyTitle("ResX Translator")]
[assembly: AssemblyDescription("Automated translation of resource (.resx) files, using Google Translate web service: http://translate.google.com/")]
[assembly: AssemblyProduct("Automated translation of resource (.resx) files, using Google Translate web service: http://translate.google.com/")]
[assembly: AssemblyCompany("Nikola Bogdanović")]
[assembly: AssemblyCopyright("© 2013-2014 http://rs.linkedin.com/in/NikolaBogdanovic")]
[assembly: AssemblyTrademark("pOcHa")]
[assembly: AssemblyCulture("")]
[assembly: NeutralResourcesLanguage("en-US")]