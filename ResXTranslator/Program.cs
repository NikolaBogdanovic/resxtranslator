﻿namespace ResXTranslator
{
    using System;
    using System.Collections;
    using System.Globalization;
    using System.Text;
    using System.Threading;
    using System.Windows.Forms;

    internal static class Program
    {
        [STAThread]
        private static void Main()
        {
            Application.ApplicationExit += ApplicationExit;
            Application.ThreadException += ThreadException;
            Application.SetUnhandledExceptionMode(UnhandledExceptionMode.CatchException);
            AppDomain.CurrentDomain.UnhandledException += UnhandledException;
            Thread.CurrentThread.CurrentUICulture = Thread.CurrentThread.CurrentCulture = CultureInfo.InvariantCulture;
            Application.EnableVisualStyles();
            Application.SetCompatibleTextRenderingDefault(false);
            Application.Run(new ResXTranslator());
        }

        private static void ApplicationExit(object sender, EventArgs e)
        {
            if(ResXTranslator.BgW != null)
                ResXTranslator.BgW.Dispose();
        }

        internal static void ShowException(Exception ex)
        {
            try
            {
                ex = ex.GetBaseException();
                var sb = new StringBuilder("Exception Message: " + ex.Message);
                if(ex.Data.Count > 0)
                {
                    sb.AppendLine();
                    sb.AppendLine();
                    sb.Append("Data Collection:");
                    foreach(DictionaryEntry de in ex.Data)
                    {
                        sb.AppendLine();
                        sb.AppendFormat(CultureInfo.InvariantCulture, "{0} = {1}", de.Key, de.Value);
                    }
                }
                if(ex.HelpLink != null)
                {
                    sb.AppendLine();
                    sb.AppendLine();
                    sb.Append("Help Link: " + ex.HelpLink);
                }
                if(ex.Source != null)
                {
                    sb.AppendLine();
                    sb.AppendLine();
                    sb.Append("Exception Source: " + ex.Source);
                }
                if(ex.TargetSite != null)
                {
                    sb.AppendLine();
                    sb.AppendLine();
                    sb.AppendLine("Target Site: " + ex.TargetSite);
                    sb.AppendLine();
                    sb.AppendLine("Stack Trace:");
                    sb.Append(ex.StackTrace);
                }
                MessageBox.Show(sb.ToString(), "ResX Translator: Internal Error!", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            finally
            {
                Environment.Exit(-1);
            }
        }

        private static void ThreadException(object sender, ThreadExceptionEventArgs e)
        {
            ShowException(e.Exception);
        }

        private static void UnhandledException(object sender, UnhandledExceptionEventArgs e)
        {
            ShowException((Exception)e.ExceptionObject);
        }
    }
}