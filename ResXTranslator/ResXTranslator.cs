﻿namespace ResXTranslator
{
    using System;
    using System.ComponentModel;
    using System.Data;
    using System.IO;
    using System.Windows.Forms;

    internal sealed partial class ResXTranslator : Form
    {
        internal static BackgroundWorker BgW { get; private set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Reliability", "CA2000:Dispose objects before losing scope")]
        internal ResXTranslator()
        {
            InitializeComponent();
            BgW = new BackgroundWorker
            {
                WorkerReportsProgress = true,
                WorkerSupportsCancellation = true
            };
            BgW.DoWork += BgW_DoWork;
            BgW.ProgressChanged += BgW_ProgressChanged;
            BgW.RunWorkerCompleted += BgW_RunWorkerCompleted;
        }

        private void ResXTranslator_Load(object sender, EventArgs e)
        {
            sources.ValueMember = targets.ValueMember = "Id";
            sources.DisplayMember = targets.DisplayMember = "Name";
            DataTable dt = GoogleTranslate.Languages();
            sources.DataSource = dt;
            sources.SelectedValue = "en";
            targets.DataSource = dt.Copy();
        }

        private void browse_CheckedChanged(object sender, EventArgs e)
        {
            ResetFiles();
        }

        private void ResetFiles()
        {
            start.Enabled = false;
            files.Items.Clear();
            status.Text = null;
        }

        private void browse_Click(object sender, EventArgs e)
        {
            if(resx.Checked)
            {
                using(var file = new OpenFileDialog())
                {
                    file.DefaultExt = "resx";
                    file.Filter = "ResX files (*.resx)|*.resx";
                    file.InitialDirectory = path.Text;
                    file.Multiselect = true;
                    file.SupportMultiDottedExtensions = true;
                    if(file.ShowDialog() == DialogResult.OK)
                    {
                        ResetFiles();
                        path.Text = Path.GetDirectoryName(file.FileName);
                        // ReSharper disable once CoVariantArrayConversion
                        files.Items.AddRange(file.FileNames);
                    }
                    else
                        return;
                }
                start.Enabled = true;
            }
            else
            {
                using(var browser = new FolderBrowserDialog())
                {
                    browser.SelectedPath = path.Text;
                    if(browser.ShowDialog() == DialogResult.OK)
                    {
                        ResetFiles();
                        path.Text = browser.SelectedPath;
                    }
                    else
                        return;
                }
                string[] names = Directory.GetFiles(path.Text, "*.resx", subs.Checked ? SearchOption.AllDirectories : SearchOption.TopDirectoryOnly);
                if(names.Length > 0)
                {
                    // ReSharper disable once CoVariantArrayConversion
                    files.Items.AddRange(names);
                    start.Enabled = true;
                }
                else
                    status.Text = "ResX files not found!";
            }
        }

        private void start_Click(object sender, EventArgs e)
        {
            start.Enabled = tab1.Enabled = tab2.Enabled = false;
            int i = files.SelectedIndices.Count;
            if(i > 0)
            {
                int j = targets.SelectedIndices.Count;
                if(j > 0)
                {
                    progress.Minimum = progress.Value = 0;
                    var names = new string[i];
                    files.SelectedItems.CopyTo(names, 0);
                    var langs = new DataRowView[j];
                    targets.SelectedItems.CopyTo(langs, 0);
                    progress.Maximum = (i * j) + 1;
                    BgW.RunWorkerAsync(
                        new[]
                        {
                            sources.SelectedValue,
                            langs,
                            latin.Checked,
                            data.Checked,
                            text.Checked,
                            dirs.Checked,
                            names
                        });
                    status.Text = "Started translating...";
                    stop.Enabled = true;
                    return;
                }
                status.Text = "Target languages not selected!";
            }
            else
                status.Text = "ResX files not selected!";
            tab2.Enabled = tab1.Enabled = start.Enabled = true;
        }

        private void stop_Click(object sender, EventArgs e)
        {
            BgW.CancelAsync();
        }

        private void status_TextChanged(object sender, EventArgs e)
        {
            status.SelectionStart = status.Text.Length;
            status.ScrollToCaret();
        }

        private static void BgW_DoWork(object sender, DoWorkEventArgs e)
        {
            int total = 0;
            var args = (object[])e.Argument;
            foreach(string file in (string[])args[6])
            {
                foreach(DataRowView drv in (DataRowView[])args[1])
                {
                    BgW.ReportProgress(++total, new[] { file, (string)drv["Name"] });
                    ResXIO.Write((string)args[0], (string)drv["Id"], (bool)args[2] && (bool)drv["Latin"], (bool)args[3], (bool)args[4], (bool)args[5], file);
                    if(!BgW.CancellationPending)
                        continue;
                    e.Cancel = true;
                    return;
                }
            }
        }

        private void BgW_ProgressChanged(object sender, ProgressChangedEventArgs e)
        {
            var args = (string[])e.UserState;
            status.Text = string.Format("{0} {1} to {2}...", status.Text, args[0], args[1]);
            progress.Value = e.ProgressPercentage;
        }

        private void BgW_RunWorkerCompleted(object sender, RunWorkerCompletedEventArgs e)
        {
            progress.Value = progress.Maximum;
            if(e.Error != null)
            {
                status.Text = "Translation failed!";
                Program.ShowException(e.Error);
            }
            else if(e.Cancelled)
                status.Text = "Stopped translating!";
            else
                status.Text = "Translation completed!";
            stop.Enabled = false;
            tab2.Enabled = tab1.Enabled = start.Enabled = true;
        }

        private void all_LinkClicked(object sender, LinkLabelLinkClickedEventArgs e)
        {
            files.BeginUpdate();
            if(files.SelectedIndices.Count < files.Items.Count)
            {
                for(int i = 0, j = files.Items.Count; i < j; i++)
                {
                    if(!files.SelectedIndices.Contains(i))
                        files.SelectedIndices.Add(i);
                }
            }
            else
                files.SelectedIndices.Clear();
            files.EndUpdate();
        }

        private void invert_LinkClicked(object sender, LinkLabelLinkClickedEventArgs e)
        {
            files.BeginUpdate();
            for(int i = 0, j = files.Items.Count; i < j; i++)
            {
                if(!files.SelectedIndices.Contains(i))
                    files.SelectedIndices.Add(i);
                else
                    files.SelectedIndices.Remove(i);
            }
            files.EndUpdate();
        }

        private void targetsAll_LinkClicked(object sender, LinkLabelLinkClickedEventArgs e)
        {
            targets.BeginUpdate();
            if(targets.SelectedIndices.Count < targets.Items.Count)
            {
                for(int i = 0, j = targets.Items.Count; i < j; i++)
                {
                    if(!targets.SelectedIndices.Contains(i))
                        targets.SelectedIndices.Add(i);
                }
            }
            else
                targets.SelectedIndices.Clear();
            targets.EndUpdate();
        }

        private void targetsInvert_LinkClicked(object sender, LinkLabelLinkClickedEventArgs e)
        {
            targets.BeginUpdate();
            for(int i = 0, j = targets.Items.Count; i < j; i++)
            {
                if(!targets.SelectedIndices.Contains(i))
                    targets.SelectedIndices.Add(i);
                else
                    targets.SelectedIndices.Remove(i);
            }
            targets.EndUpdate();
        }
    }
}