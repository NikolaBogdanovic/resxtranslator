﻿namespace ResXTranslator
{
    internal sealed partial class ResXTranslator
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if(disposing && components != null)
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.tabs = new System.Windows.Forms.TabControl();
            this.tab1 = new System.Windows.Forms.TabPage();
            this.filesAll = new System.Windows.Forms.LinkLabel();
            this.filesInvert = new System.Windows.Forms.LinkLabel();
            this.label5 = new System.Windows.Forms.Label();
            this.files = new System.Windows.Forms.ListBox();
            this.label4 = new System.Windows.Forms.Label();
            this.data = new System.Windows.Forms.CheckBox();
            this.label3 = new System.Windows.Forms.Label();
            this.subs = new System.Windows.Forms.RadioButton();
            this.text = new System.Windows.Forms.CheckBox();
            this.folder = new System.Windows.Forms.RadioButton();
            this.resx = new System.Windows.Forms.RadioButton();
            this.browse = new System.Windows.Forms.Button();
            this.path = new System.Windows.Forms.TextBox();
            this.tab2 = new System.Windows.Forms.TabPage();
            this.targetsAll = new System.Windows.Forms.LinkLabel();
            this.targetsInvert = new System.Windows.Forms.LinkLabel();
            this.label6 = new System.Windows.Forms.Label();
            this.targets = new System.Windows.Forms.ListBox();
            this.latin = new System.Windows.Forms.CheckBox();
            this.sources = new System.Windows.Forms.ComboBox();
            this.dirs = new System.Windows.Forms.CheckBox();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.stop = new System.Windows.Forms.Button();
            this.start = new System.Windows.Forms.Button();
            this.strip = new System.Windows.Forms.StatusStrip();
            this.progress = new System.Windows.Forms.ToolStripProgressBar();
            this.status = new System.Windows.Forms.TextBox();
            this.tabs.SuspendLayout();
            this.tab1.SuspendLayout();
            this.tab2.SuspendLayout();
            this.strip.SuspendLayout();
            this.SuspendLayout();
            // 
            // tabs
            // 
            this.tabs.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.tabs.Controls.Add(this.tab1);
            this.tabs.Controls.Add(this.tab2);
            this.tabs.Location = new System.Drawing.Point(6, 6);
            this.tabs.Name = "tabs";
            this.tabs.SelectedIndex = 0;
            this.tabs.Size = new System.Drawing.Size(618, 356);
            this.tabs.TabIndex = 0;
            // 
            // tab1
            // 
            this.tab1.Controls.Add(this.filesAll);
            this.tab1.Controls.Add(this.filesInvert);
            this.tab1.Controls.Add(this.label5);
            this.tab1.Controls.Add(this.files);
            this.tab1.Controls.Add(this.label4);
            this.tab1.Controls.Add(this.data);
            this.tab1.Controls.Add(this.label3);
            this.tab1.Controls.Add(this.subs);
            this.tab1.Controls.Add(this.text);
            this.tab1.Controls.Add(this.folder);
            this.tab1.Controls.Add(this.resx);
            this.tab1.Controls.Add(this.browse);
            this.tab1.Controls.Add(this.path);
            this.tab1.Location = new System.Drawing.Point(4, 22);
            this.tab1.Name = "tab1";
            this.tab1.Padding = new System.Windows.Forms.Padding(3);
            this.tab1.Size = new System.Drawing.Size(610, 330);
            this.tab1.TabIndex = 0;
            this.tab1.Text = "Resources";
            this.tab1.UseVisualStyleBackColor = true;
            // 
            // filesAll
            // 
            this.filesAll.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.filesAll.AutoSize = true;
            this.filesAll.Location = new System.Drawing.Point(546, 8);
            this.filesAll.Name = "filesAll";
            this.filesAll.Size = new System.Drawing.Size(18, 13);
            this.filesAll.TabIndex = 20;
            this.filesAll.TabStop = true;
            this.filesAll.Text = "All";
            this.filesAll.LinkClicked += new System.Windows.Forms.LinkLabelLinkClickedEventHandler(this.all_LinkClicked);
            // 
            // filesInvert
            // 
            this.filesInvert.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.filesInvert.AutoSize = true;
            this.filesInvert.Location = new System.Drawing.Point(570, 8);
            this.filesInvert.Name = "filesInvert";
            this.filesInvert.Size = new System.Drawing.Size(34, 13);
            this.filesInvert.TabIndex = 19;
            this.filesInvert.TabStop = true;
            this.filesInvert.Text = "Invert";
            this.filesInvert.LinkClicked += new System.Windows.Forms.LinkLabelLinkClickedEventHandler(this.invert_LinkClicked);
            // 
            // label5
            // 
            this.label5.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label5.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.label5.Location = new System.Drawing.Point(6, 308);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(54, 13);
            this.label5.TabIndex = 18;
            this.label5.Text = "Options:";
            // 
            // files
            // 
            this.files.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.files.HorizontalScrollbar = true;
            this.files.Location = new System.Drawing.Point(6, 66);
            this.files.Name = "files";
            this.files.SelectionMode = System.Windows.Forms.SelectionMode.MultiExtended;
            this.files.Size = new System.Drawing.Size(598, 225);
            this.files.Sorted = true;
            this.files.TabIndex = 17;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.label4.Location = new System.Drawing.Point(6, 8);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(41, 13);
            this.label4.TabIndex = 16;
            this.label4.Text = "ResX:";
            // 
            // data
            // 
            this.data.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.data.AutoSize = true;
            this.data.Checked = true;
            this.data.CheckState = System.Windows.Forms.CheckState.Checked;
            this.data.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.data.Location = new System.Drawing.Point(66, 307);
            this.data.Name = "data";
            this.data.Size = new System.Drawing.Size(98, 17);
            this.data.TabIndex = 15;
            this.data.Text = "New Data Only";
            // 
            // label3
            // 
            this.label3.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.label3.Location = new System.Drawing.Point(493, 8);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(47, 13);
            this.label3.TabIndex = 14;
            this.label3.Text = "Select:";
            // 
            // subs
            // 
            this.subs.AutoSize = true;
            this.subs.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.subs.Location = new System.Drawing.Point(165, 6);
            this.subs.Name = "subs";
            this.subs.Size = new System.Drawing.Size(65, 17);
            this.subs.TabIndex = 13;
            this.subs.TabStop = true;
            this.subs.Text = "Sub Dirs";
            this.subs.CheckedChanged += new System.EventHandler(this.browse_CheckedChanged);
            // 
            // text
            // 
            this.text.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.text.AutoSize = true;
            this.text.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.text.Location = new System.Drawing.Point(170, 307);
            this.text.Name = "text";
            this.text.Size = new System.Drawing.Size(97, 17);
            this.text.TabIndex = 13;
            this.text.Text = "Text Keys Only";
            // 
            // folder
            // 
            this.folder.AutoSize = true;
            this.folder.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.folder.Location = new System.Drawing.Point(105, 6);
            this.folder.Name = "folder";
            this.folder.Size = new System.Drawing.Size(54, 17);
            this.folder.TabIndex = 12;
            this.folder.TabStop = true;
            this.folder.Text = "Folder";
            this.folder.CheckedChanged += new System.EventHandler(this.browse_CheckedChanged);
            // 
            // resx
            // 
            this.resx.AutoSize = true;
            this.resx.Checked = true;
            this.resx.Location = new System.Drawing.Point(53, 6);
            this.resx.Name = "resx";
            this.resx.Size = new System.Drawing.Size(46, 17);
            this.resx.TabIndex = 11;
            this.resx.TabStop = true;
            this.resx.Text = "Files";
            this.resx.CheckedChanged += new System.EventHandler(this.browse_CheckedChanged);
            // 
            // browse
            // 
            this.browse.Location = new System.Drawing.Point(529, 30);
            this.browse.Name = "browse";
            this.browse.Size = new System.Drawing.Size(75, 23);
            this.browse.TabIndex = 3;
            this.browse.Text = "Browse";
            this.browse.Click += new System.EventHandler(this.browse_Click);
            // 
            // path
            // 
            this.path.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.path.Location = new System.Drawing.Point(6, 32);
            this.path.Name = "path";
            this.path.ReadOnly = true;
            this.path.Size = new System.Drawing.Size(517, 20);
            this.path.TabIndex = 0;
            // 
            // tab2
            // 
            this.tab2.Controls.Add(this.targetsAll);
            this.tab2.Controls.Add(this.targetsInvert);
            this.tab2.Controls.Add(this.label6);
            this.tab2.Controls.Add(this.targets);
            this.tab2.Controls.Add(this.latin);
            this.tab2.Controls.Add(this.sources);
            this.tab2.Controls.Add(this.dirs);
            this.tab2.Controls.Add(this.label1);
            this.tab2.Controls.Add(this.label2);
            this.tab2.Location = new System.Drawing.Point(4, 22);
            this.tab2.Name = "tab2";
            this.tab2.Padding = new System.Windows.Forms.Padding(3);
            this.tab2.Size = new System.Drawing.Size(610, 330);
            this.tab2.TabIndex = 1;
            this.tab2.Text = "Languages";
            this.tab2.UseVisualStyleBackColor = true;
            // 
            // targetsAll
            // 
            this.targetsAll.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.targetsAll.AutoSize = true;
            this.targetsAll.Location = new System.Drawing.Point(546, 9);
            this.targetsAll.Name = "targetsAll";
            this.targetsAll.Size = new System.Drawing.Size(18, 13);
            this.targetsAll.TabIndex = 22;
            this.targetsAll.TabStop = true;
            this.targetsAll.Text = "All";
            this.targetsAll.LinkClicked += new System.Windows.Forms.LinkLabelLinkClickedEventHandler(this.targetsAll_LinkClicked);
            // 
            // targetsInvert
            // 
            this.targetsInvert.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.targetsInvert.AutoSize = true;
            this.targetsInvert.Location = new System.Drawing.Point(570, 9);
            this.targetsInvert.Name = "targetsInvert";
            this.targetsInvert.Size = new System.Drawing.Size(34, 13);
            this.targetsInvert.TabIndex = 21;
            this.targetsInvert.TabStop = true;
            this.targetsInvert.Text = "Invert";
            this.targetsInvert.LinkClicked += new System.Windows.Forms.LinkLabelLinkClickedEventHandler(this.targetsInvert_LinkClicked);
            // 
            // label6
            // 
            this.label6.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.label6.AutoSize = true;
            this.label6.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label6.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.label6.Location = new System.Drawing.Point(6, 308);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(54, 13);
            this.label6.TabIndex = 19;
            this.label6.Text = "Options:";
            // 
            // targets
            // 
            this.targets.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.targets.HorizontalScrollbar = true;
            this.targets.Location = new System.Drawing.Point(6, 40);
            this.targets.MultiColumn = true;
            this.targets.Name = "targets";
            this.targets.SelectionMode = System.Windows.Forms.SelectionMode.MultiExtended;
            this.targets.Size = new System.Drawing.Size(598, 251);
            this.targets.Sorted = true;
            this.targets.TabIndex = 18;
            // 
            // latin
            // 
            this.latin.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.latin.AutoSize = true;
            this.latin.Checked = true;
            this.latin.CheckState = System.Windows.Forms.CheckState.Checked;
            this.latin.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.latin.Location = new System.Drawing.Point(66, 307);
            this.latin.Name = "latin";
            this.latin.Size = new System.Drawing.Size(79, 17);
            this.latin.TabIndex = 5;
            this.latin.Text = "Latin Script";
            // 
            // sources
            // 
            this.sources.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.sources.Location = new System.Drawing.Point(63, 6);
            this.sources.Name = "sources";
            this.sources.Size = new System.Drawing.Size(210, 21);
            this.sources.Sorted = true;
            this.sources.TabIndex = 4;
            // 
            // dirs
            // 
            this.dirs.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.dirs.AutoSize = true;
            this.dirs.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.dirs.Location = new System.Drawing.Point(151, 307);
            this.dirs.Name = "dirs";
            this.dirs.Size = new System.Drawing.Size(106, 17);
            this.dirs.TabIndex = 14;
            this.dirs.Text = "Separate Folders";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.label1.Location = new System.Drawing.Point(6, 9);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(51, 13);
            this.label1.TabIndex = 3;
            this.label1.Text = "Source:";
            // 
            // label2
            // 
            this.label2.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.Location = new System.Drawing.Point(486, 9);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(54, 13);
            this.label2.TabIndex = 2;
            this.label2.Text = "Targets:";
            // 
            // stop
            // 
            this.stop.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.stop.Enabled = false;
            this.stop.Location = new System.Drawing.Point(549, 397);
            this.stop.Name = "stop";
            this.stop.Size = new System.Drawing.Size(75, 23);
            this.stop.TabIndex = 11;
            this.stop.Text = "Stop";
            this.stop.Click += new System.EventHandler(this.stop_Click);
            // 
            // start
            // 
            this.start.Anchor = System.Windows.Forms.AnchorStyles.Bottom;
            this.start.Enabled = false;
            this.start.Location = new System.Drawing.Point(549, 368);
            this.start.Name = "start";
            this.start.Size = new System.Drawing.Size(75, 23);
            this.start.TabIndex = 5;
            this.start.Text = "Start";
            this.start.Click += new System.EventHandler(this.start_Click);
            // 
            // strip
            // 
            this.strip.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.progress});
            this.strip.Location = new System.Drawing.Point(3, 423);
            this.strip.Name = "strip";
            this.strip.ShowItemToolTips = true;
            this.strip.Size = new System.Drawing.Size(624, 22);
            this.strip.SizingGrip = false;
            this.strip.TabIndex = 5;
            // 
            // progress
            // 
            this.progress.Name = "progress";
            this.progress.Size = new System.Drawing.Size(620, 16);
            // 
            // status
            // 
            this.status.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.status.Location = new System.Drawing.Point(6, 368);
            this.status.Multiline = true;
            this.status.Name = "status";
            this.status.ReadOnly = true;
            this.status.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.status.Size = new System.Drawing.Size(537, 52);
            this.status.TabIndex = 12;
            this.status.TextChanged += new System.EventHandler(this.status_TextChanged);
            // 
            // ResXTranslator
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(630, 448);
            this.Controls.Add(this.status);
            this.Controls.Add(this.strip);
            this.Controls.Add(this.tabs);
            this.Controls.Add(this.start);
            this.Controls.Add(this.stop);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.Fixed3D;
            this.MaximizeBox = false;
            this.Name = "ResXTranslator";
            this.Padding = new System.Windows.Forms.Padding(3);
            this.Text = "ResX Translator";
            this.Load += new System.EventHandler(this.ResXTranslator_Load);
            this.tabs.ResumeLayout(false);
            this.tab1.ResumeLayout(false);
            this.tab1.PerformLayout();
            this.tab2.ResumeLayout(false);
            this.tab2.PerformLayout();
            this.strip.ResumeLayout(false);
            this.strip.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.TabControl tabs;
        private System.Windows.Forms.TabPage tab1;
        private System.Windows.Forms.TabPage tab2;
        private System.Windows.Forms.Button browse;
        private System.Windows.Forms.TextBox path;
        private System.Windows.Forms.Button start;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.StatusStrip strip;
        private System.Windows.Forms.Button stop;
        private System.Windows.Forms.CheckBox text;
        private System.Windows.Forms.CheckBox dirs;
        private System.Windows.Forms.CheckBox data;
        private System.Windows.Forms.ComboBox sources;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.CheckBox latin;
        private System.Windows.Forms.RadioButton resx;
        private System.Windows.Forms.RadioButton folder;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.RadioButton subs;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.ListBox files;
        private System.Windows.Forms.ListBox targets;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.ToolStripProgressBar progress;
        private System.Windows.Forms.TextBox status;
        private System.Windows.Forms.LinkLabel filesAll;
        private System.Windows.Forms.LinkLabel filesInvert;
        private System.Windows.Forms.LinkLabel targetsAll;
        private System.Windows.Forms.LinkLabel targetsInvert;
    }
}