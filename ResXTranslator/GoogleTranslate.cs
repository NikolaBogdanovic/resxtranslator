﻿namespace ResXTranslator
{
    using System;
    using System.Data;
    using System.Globalization;
    using System.IO;
    using System.Net;
    using System.Net.Mime;
    using System.Text;
    using System.Xml;

    internal static class GoogleTranslate
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Reliability", "CA2000:Dispose objects before losing scope")]
        internal static DataTable Languages()
        {
            var dt = new DataTable
            {
                CaseSensitive = true,
                Locale = CultureInfo.InvariantCulture
            };
            dt.Columns.Add("Id", typeof(string));
            dt.Columns.Add("Name", typeof(string));
            dt.Columns.Add("Latin", typeof(bool));
            dt.PrimaryKey = new[]
            {
                dt.Columns["Id"]
            };
            dt.BeginLoadData();
            using(var reader = XmlReader.Create(
                new FileStream("Languages.xml", FileMode.Open, FileAccess.Read, FileShare.Read),
                new XmlReaderSettings
                {
                    CloseInput = true,
                    DtdProcessing = DtdProcessing.Ignore,
                    IgnoreComments = true,
                    IgnoreProcessingInstructions = true,
                    IgnoreWhitespace = true
                }))
            {
                if(reader.ReadToFollowing("Languages") && reader.ReadToDescendant("Language"))
                {
                    do
                    {
                        dt.Rows.Add(
                            reader["Id"],
                            reader["Name"],
                            reader["Latin"] != null && Convert.ToBoolean(reader["Latin"]));
                    }
                    while(reader.ReadToNextSibling("Language"));
                }
            }
            dt.EndLoadData();
            return dt;
        }

        internal static string Translate(string source, string target, bool latin, string text)
        {
            string rsp;
            try
            {
                byte[] data;
                ContentType type;
                Encoding enc = Encoding.UTF8;
                using(var wc = new WebClient())
                {
                    wc.Encoding = enc;
                    data = wc.DownloadData(string.Format("http://translate.google.com/?ie=UTF8&hl=en&sl={0}&tl={1}&text={2}", source, target, text));
                    WebHeaderCollection headers = wc.ResponseHeaders;
                    type = headers != null && !string.IsNullOrEmpty(headers["Content-Type"]) ? new ContentType(headers["Content-Type"]) : null;
                }
                if(type != null && !string.IsNullOrEmpty(type.CharSet))
                    enc = Encoding.GetEncoding(type.CharSet);
                rsp = enc.GetString(data);
                if(latin)
                {
                    const string Html = " id=res-translit ";
                    int i = rsp.IndexOf(Html, StringComparison.Ordinal);
                    if(i == -1)
                    {
                        const string Xml = " id=\"res-translit\"";
                        i = rsp.IndexOf(Xml, StringComparison.Ordinal);
                        if(i == -1)
                            return null;
                        i += Xml.Length;
                    }
                    else
                        i += Html.Length;
                    rsp = rsp.Substring(rsp.IndexOf('>', i) + 1);
                    rsp = rsp.Substring(0, rsp.IndexOf("</", StringComparison.Ordinal));
                }
                else
                {
                    const string Js = ";TRANSLATED_TEXT='";
                    int i = rsp.IndexOf(Js, StringComparison.Ordinal);
                    if(i == -1)
                        return null;
                    i += Js.Length;
                    rsp = rsp.Substring(i);
                    rsp = rsp.Substring(0, rsp.IndexOf("';", StringComparison.Ordinal));
                }
            }
            catch(Exception ex)
            {
                Program.ShowException(ex);
                return null;
            }
            return WebUtility.HtmlDecode(rsp);
        }
    }
}