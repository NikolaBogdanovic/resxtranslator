﻿namespace ResXTranslator
{
    using System;
    using System.Collections;
    using System.Collections.Generic;
    using System.ComponentModel.Design;
    using System.IO;
    using System.Resources;
    using System.Text;
    using System.Threading;

    // ReSharper disable once InconsistentNaming
    internal static class ResXIO
    {
        private static readonly string StringQualifiedName = typeof(string).AssemblyQualifiedName;

        internal static void Write(string source, string target, bool latin, bool data, bool text, bool dirs, string file)
        {
            string path;
            if(dirs)
            {
                // ReSharper disable once AssignNullToNotNullAttribute
                path = Path.Combine(Path.GetDirectoryName(file), target);
                if(!Directory.Exists(path))
                    Directory.CreateDirectory(path);
            }
            else
                path = Path.GetDirectoryName(file);
            // ReSharper disable once AssignNullToNotNullAttribute
            string name = Path.Combine(path, string.Format("{0}.{1}.resx", Path.GetFileNameWithoutExtension(file), target));
            Dictionary<string, ResXDataNode> old = null;
            if(File.Exists(name))
            {
                old = new Dictionary<string, ResXDataNode>();
                using(var reader = new ResXResourceReader(name))
                {
                    reader.UseResXDataNodes = true;
                    foreach(DictionaryEntry de in reader)
                        old.Add((string)de.Key, (ResXDataNode)de.Value);
                }
            }
            using(var writer = new ResXResourceWriter(name))
            {
                using(var reader = new ResXResourceReader(file))
                {
                    reader.UseResXDataNodes = true;
                    var sb = new StringBuilder();
                    foreach(DictionaryEntry de in reader)
                    {
                        if(ResXTranslator.BgW.CancellationPending)
                            return;
                        var key = (string)de.Key;
                        if(old != null && old.ContainsKey(key))
                        {
                            writer.AddResource(key, old[key]);
                            if(data)
                                continue;
                        }
                        if(text && !key.EndsWith(".Text", StringComparison.Ordinal))
                            continue;
                        var node = (ResXDataNode)de.Value;
                        if(node.FileRef != null || node.GetValueTypeName((ITypeResolutionService)null) != StringQualifiedName)
                            continue;
                        var val = node.GetValue((ITypeResolutionService)null) as string;
                        if(string.IsNullOrEmpty(val))
                            continue;
                        sb.Clear();
                        val = Uri.EscapeDataString(val);
                        while(val.Length > 1928)
                        {
                            var index = val.LastIndexOf(' ', 1928);
                            if(index == -1)
                                index = 1928;
                            string sub = val.Substring(0, index);
                            sb.Append(GoogleTranslate.Translate(source, target, latin, sub));
                            val = val.Substring(sub.Length);
                            Thread.Sleep(500);
                        }
                        sb.Append(GoogleTranslate.Translate(source, target, latin, val));
                        writer.AddResource(key, sb.ToString());
                        Thread.Sleep(500);
                    }
                }
                writer.Generate();
            }
        }
    }
}